﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportShell.Models
{
    public sealed class ShellFuelTransaction
    {
        public sealed class Root
        {
            public Client Client { get; set; }
            public Car Car { get; set; }
            public Transaction Transaction { get; set; }
            public Place Place { get; set; }
            public FuelInvoice FuelInvoice { get; set; }
            public Root()
            {
                Client = new Client();
                Car = new Car();
                Transaction = new Transaction();
                Place = new Place();
                FuelInvoice = new FuelInvoice();
            }            
        }

        public sealed class Client
        {
            public string Id { get; set; }
            public string PayerName { get; set; }
            public string ClientName { get; set; }
            public string ClientNumber { get; set; }
            public string ShortClientNumber { get; set; }
        }

        public sealed class Car
        {
            public string CarRegistration { get; set; }
            public string DriverLastname { get; set; }
        }

        public sealed class Transaction
        {
            public string Date { get; set; }
            public string Time { get; set; }
        }

        public sealed class Place
        {
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public sealed class FuelInvoice
        {
            public FuelType FuelType { get; set; }
            public double Quantity { get; set; }
            public string Currency { get; set; }
            public double UnitPrice { get; set; }
            public double UnitDiscount { get; set; }
            public double DeliveryDiscount { get; set; }
            public string DiscountGroup { get; set; }
            public double Tax { get; set; }
            public double NettoTotal { get; set; }
            public bool Invoiced { get; set; }
            public string InvoiceNumber { get; set; }
        }

        public enum FuelType
        {
            On = 0, OnExtra = 1, OnVPower = 2 , Pb95 = 3, Pb98 = 4, Pb98VPower = 5, Lpg = 6
        }
    }
}
