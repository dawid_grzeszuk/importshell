﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportShell.Models;

namespace ImportShell.Fliters
{
    public sealed class ShellFliter
    {
        public ShellFuelTransaction.FuelType FuelType { get; set; }
        public string CarRegistration { get; set; }
        public string PetrolStationLocation { get; set; }
        public string DriverLastname { get; set; }
        public bool CanFliter() => typeof(ShellFliter).GetProperties().Any(property => !string.IsNullOrEmpty(property.GetValue(typeof(ShellFliter)).ToString()));
    }
}
