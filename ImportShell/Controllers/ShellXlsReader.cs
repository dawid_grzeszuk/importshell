﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportShell.Fliters;
using ImportShell.Interfaces;
using ImportShell.Models;
using OfficeOpenXml;

namespace ImportShell.Controllers
{
    public sealed class ShellXlsReader : IShellXlsReader
    {
        #region File Loading
        //Sprawdzamy czy ścieżka nie jest pusta i czy prowadzi do pliku
        public ExcelPackage Read(string path) => File.Exists(path) ? new ExcelPackage(File.OpenRead(path)) : null;
        #endregion

        #region File Reading

        public IEnumerable<ShellFuelTransaction.Root> MapToObject(ExcelPackage file)
        {
            //Jeżeli nie ma pliku kończymy działanie funkcji
            if (Equals(file, null)) return null;

            //Tworzymy liste transakcji
            var transactions = new List<ShellFuelTransaction.Root>();

            using (var worksheet = file.Workbook.Worksheets[1])
            {
                for (var row = 2; row < worksheet.Dimension.Rows + 1; row++)
                {
                    var value = worksheet.Cells[row,11].Value;
                    if(value != null && (value.ToString().ToLower().Contains("podatek") || value.ToString().ToLower().Contains("autostrada") 
                                                                                        || value.ToString().ToLower().Contains("silnikowe")
                                                                                        || value.ToString().ToLower().Contains("kosmetyki"))) continue;

                    var tempTranscation = new ShellFuelTransaction.Root();

                    for (var column = 1; column < worksheet.Dimension.Columns + 1; column++)
                    {

                        if (Equals(worksheet.Cells[row, column].Value, null)) continue;
                        if (string.IsNullOrEmpty(worksheet.Cells[row, column].Value.ToString())) continue;

                        switch (column)
                        {
                            case 1:
                                tempTranscation.Client.Id = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 2:
                                tempTranscation.Client.PayerName = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 3:
                                tempTranscation.Client.ClientName = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 4:
                                tempTranscation.Client.ClientNumber = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 5:
                                tempTranscation.Client.ShortClientNumber = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 6:
                                tempTranscation.Car.CarRegistration = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 7:
                                tempTranscation.Car.DriverLastname = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 8:
                                tempTranscation.Transaction.Date = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 9:
                                tempTranscation.Transaction.Time = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 10:
                                tempTranscation.Place.Name = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 11:
                                var fuel = worksheet.Cells[row, column].Value.ToString().ToLower();

                                if (fuel.Contains("v-power"))
                                {
                                    tempTranscation.FuelInvoice.FuelType = (fuel.Contains("olej"))
                                        ? ShellFuelTransaction.FuelType.OnVPower
                                        : ShellFuelTransaction.FuelType.Pb98VPower;
                                }
                                else if (fuel.Contains("olej"))           
                                    tempTranscation.FuelInvoice.FuelType = ShellFuelTransaction.FuelType.OnExtra;                              
                                else if (fuel.Contains("98"))                            
                                    tempTranscation.FuelInvoice.FuelType = ShellFuelTransaction.FuelType.Pb98;                              
                                else if (fuel.Contains("95"))                               
                                    tempTranscation.FuelInvoice.FuelType = ShellFuelTransaction.FuelType.Pb95;                              
                                else                                 
                                    tempTranscation.FuelInvoice.FuelType = ShellFuelTransaction.FuelType.Lpg;                               
                                break;

                            case 12:
                                tempTranscation.FuelInvoice.Quantity = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 13:
                                tempTranscation.FuelInvoice.Currency = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 14:
                                tempTranscation.FuelInvoice.UnitPrice = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 15:
                                tempTranscation.FuelInvoice.UnitDiscount = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 16:
                                tempTranscation.FuelInvoice.DeliveryDiscount = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 17:
                                tempTranscation.FuelInvoice.Tax = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 18:
                                tempTranscation.FuelInvoice.NettoTotal = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;

                            case 19:
                                tempTranscation.FuelInvoice.Invoiced = (worksheet.Cells[row, column].Value.ToString() == "y");
                                break;

                            case 20:
                                tempTranscation.Place.Code = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 21:
                                tempTranscation.FuelInvoice.InvoiceNumber = worksheet.Cells[row, column].Value.ToString();
                                break;

                            case 22:
                                tempTranscation.FuelInvoice.DiscountGroup = worksheet.Cells[row, column].Value.ToString();
                                break;

                            default:
                                break;
                        }
                    }
                   transactions.Add(tempTranscation);
                }
            }
            return transactions;
        }


        #endregion

        #region File Flitering
        public IEnumerable<ShellFuelTransaction.Root> Filter(IEnumerable<ShellFuelTransaction.Root> transactions, ShellFliter fliter)
        {
            var enumerable = transactions as ShellFuelTransaction.Root[] ?? transactions.ToArray();

            if (!enumerable.Any()) return null;
            if (!fliter.CanFliter()) return enumerable;

            var fliterList = enumerable.ToList();

            foreach (var transaction in fliterList)
            {
                if (!string.IsNullOrEmpty(fliter.DriverLastname) && fliter.DriverLastname != transaction.Car.DriverLastname)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.CarRegistration) && fliter.CarRegistration != transaction.Car.CarRegistration)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.PetrolStationLocation) && fliter.PetrolStationLocation != transaction.Place.Name)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.FuelType.ToString()) && fliter.FuelType.ToString() != transaction.FuelInvoice.FuelType.ToString())
                {
                    fliterList.Remove(transaction);
                }
            }
            return fliterList;
        }
        #endregion
    }
}
