﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportShell.Fliters;
using ImportShell.Models;
using OfficeOpenXml;

namespace ImportShell.Interfaces
{
    public interface IShellXlsReader
    {
        ExcelPackage Read(string path);
        IEnumerable<ShellFuelTransaction.Root> MapToObject(ExcelPackage file);
        IEnumerable<ShellFuelTransaction.Root> Filter(IEnumerable<ShellFuelTransaction.Root> transactions, ShellFliter fliter);
    }
}
